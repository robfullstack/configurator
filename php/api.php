<?php
// Set this file to be accessible
header('Access-Control-Allow-Origin: *');

// Get All Folders. Split them by line
$folders 	   = file_get_contents('../Configurations/FolderNames.txt');
$configurators = explode("\n", $folders);

// Prepare an API call data handler;
$folder_data = array();
$api_data    = array();

// Check if all folders exists
foreach ( $configurators as $folder ) {
	$folder_inner_data = trim($folder);

	$folder_inner_data = explode("|", $folder_inner_data);
	$folder_name = trim($folder_inner_data[0]);
	$folder_slug = trim($folder_inner_data[1]);

	$path = '../Configurations/' . $folder_name;
	if ( file_exists( $path ) ) {
		$style = file_get_contents($path . '/StyleNames.txt');
		$style = preg_replace('/\s+/', '', $style);
		$slug = explode("|", $folder);
		array_push( $folder_data, array(
				'name' => $folder_name,
				'slug' => $folder_slug,
				'style' => $style,
				'exists' => 1, 
				'thumb' =>
				'Configurations/' . $folder_name . '/Thumbnail.jpg'
			)
		);
	} else {
		array_push( $folder_data, array(
				'name' => $folder_name,
				'slug' => $folder_slug,
				'exists' => 0
			)
		);
	}
}

// Re-iterate each configurations
// if 'exists' flag returns true,
// get all additional configurations
foreach ( $folder_data as $config ) {
	if ( $config['exists'] ) {

		// Parsing XML into Array
		$xml  = file_get_contents('../Configurations/'. $config['name'] .'/' . $config['style'] . '/' . $config['style'] . '_.xml');
		$ob   = simplexml_load_string($xml);
		$json = json_encode($ob);
		$data = json_decode($json, true);

		// Units, Worktop and Floors Data
		$options = $data['COLIMO_OBJ'];

		$data_arr = array();

		$has_overlay = false;

		$main_index = 0;

		foreach ( $options as $opt ) {
			$config['configs'] = array(
				'name' => $opt['COLIMO_OBJ_NAME'],
				'id' => $opt['COLIMO_OBJ_ID'],
				'active' => $main_index === 0 ? true : false,
				'isOverlay' => filter_var($opt['COLIMO_OBJ_OVERLAY'], FILTER_VALIDATE_BOOLEAN),
				'isMultiple' => isset($opt['COLIMO_OBJ_MULTIPLE_OVERLAY']) ? filter_var($opt['COLIMO_OBJ_MULTIPLE_OVERLAY'], FILTER_VALIDATE_BOOLEAN) : false
			);

			$main_index++;

			// As long as $has_overlay is false,
			// check if the xml block is actually an overlay block
			if (!$has_overlay)
				$has_overlay = filter_var($opt['COLIMO_OBJ_OVERLAY'], FILTER_VALIDATE_BOOLEAN);

			if ( isset( $opt['COLIMO_MATERIAL'] ) && !empty( $opt['COLIMO_MATERIAL'] ) ) {
				$index = 0;
				foreach ( $opt['COLIMO_MATERIAL'] as $swatch ) { 
					$config['configs']['material'][] = array(
						'name' => $swatch['COLIMO_MATERIAL_NAME'],
						'material_id' => $swatch['COLIMO_MATERIAL_ID'],
						'price' => $swatch['COLIMO_MATERIAL_PRICE'],
						'excluded_overlays' => isset($swatch['COLIMO_EXCLUDE_OVERLAY']) ? explode(',', $swatch['COLIMO_EXCLUDE_OVERLAY']) : null,
						'disable_item_overlay' => isset($swatch['COLIMO_DISABLE_ITEM_OVERLAY']) ? explode(',', $swatch['COLIMO_DISABLE_ITEM_OVERLAY']) : null,
						'active' => $index === 0 ? true : false
					);
					$index++;
				}
			}
			array_push( $data_arr, $config['configs'] );
		}

		$capsule = array(
			'name' => $config['name'],
			'slug' => $config['slug'],
			'style' => $config['style'],
			'thumb' => $config['thumb'],
			'has_overlay' => $has_overlay,
			'preloaded' => false,
			'configs' => $data_arr
		);

		// Insert Data to the main api data wrapper
		array_push($api_data, $capsule);
	}
}

// Return API data
echo json_encode($api_data, JSON_PRETTY_PRINT);
// $file = fopen('data.json', 'w') or die('Unable to open file!');
// fwrite($file, json_encode($new_data, JSON_PRETTY_PRINT));
// fclose();

die();