module.exports = {
  baseUrl: process.env.NODE_ENV === "production" ? "./" : "/",
  devServer: {
    proxy: "http://127.0.0.1:2222/"
  }
};
