import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    app_preloaded: false,
    is_configurator_fetched: false,
    is_configurator_selected: false,
    is_configurator_undefined: true,
    configurations: [],
    configurator: {},
    configurator_overlay: false,
    social: {},
    social_title: "",
    type: ""
  },

  mutations: {
    SET_CONFIGURATION: function(state, payload) {
      state.configurations = payload;
      state.is_configurator_fetched = true;
    },
    SET_CONFIGURATOR: function(state, payload) {
      const selected_configurator = state.configurations.find(
        item => item.slug === payload.type
      );

      function get_all_combinations(id, haystack) {
        let result = [];
        for (let i = 0; i < haystack.length; i++) {
          result.push(id + haystack[i].material_id);
        }
        return result;
      }

      if (payload.base) {
        const combination = payload.base.match(/\d{1,3}\D{1}/g);
        let is_truth = true;
        combination.forEach(combi => {
          const id = combi.match(/\D{1}/g)[0];
          const original_id = combi.match(/\d{1,2}/g)[0];
          const index = parseInt(original_id) - 1;

          if (
            !get_all_combinations(
              original_id,
              selected_configurator.configs[index].material
            ).includes(combi)
          ) {
            is_truth = false;
            return;
          }

          selected_configurator.configs[index].material.map(mat => {
            mat.active = mat.material_id === id ? true : false;
            return mat;
          });
        });
        if (!is_truth) {
          alert(
            "One of the combinations is incorrect. Please check your link."
          );
        }
      }

      if (payload.overlay) {
        const combination = payload.overlay.match(/\d{1,3}\D{1}/g);
        let is_truth = true;
        combination.forEach(combi => {
          const id = combi.match(/\D{1}/g)[0];
          const original_id = combi.match(/\d{1,2}/g)[0];
          const index = parseInt(original_id) - 1;

          if (
            !get_all_combinations(
              original_id,
              selected_configurator.configs[index].material
            ).includes(combi)
          ) {
            is_truth = false;
            return;
          }

          selected_configurator.configs[index].material.map(mat => {
            mat.active = mat.material_id === id ? true : false;
            return mat;
          });
        });
        if (!is_truth) {
          alert(
            "One of the combinations is incorrect. Please check your link."
          );
        }
      }

      if (typeof selected_configurator !== "undefined") {
        state.configurator = selected_configurator;
        state.configurator_overlay = selected_configurator.has_overlay;
        state.is_configurator_selected = true;
        state.is_configurator_undefined = false;
      } else {
        state.configurator = undefined;
        state.is_configurator_undefined = true;
      }
    },
    SET_TYPE: function(state, payload) {
      state.type = payload;
    },
    SET_SOCIAL: function(state, payload) {
      state.social = payload;
      state.social_title = payload.title;
    },
    SET_ACTIVE: function(state, payload) {
      state.configurator.configs = state.configurator.configs.map(item => {
        return {
          ...item,
          active: item.id === payload
        };
      });
    },
    SET_APP_PRELOADED: function(state, payload) {
      if (state.configurator.preloaded) return false;
      state.app_preloaded = payload;
      state.configurator.preloaded = payload;
    },
    CHANGE_COMBINATION: function(state, payload) {
      const id = parseInt(payload.id);
      const index = state.configurator.configs.findIndex(config => {
        return parseInt(config.id) === id;
      });
      state.configurator.configs[index].material.map(mat => {
        mat.active = mat.material_id === payload.material_id ? true : false;
        return mat;
      });

    },
    ADD_OVERLAY_ITEM: function(state, payload) {
      const moverlay_id = parseInt(payload.id);
      const isActive = payload.isactive;
      const index = state.configurator.configs.findIndex(config => {
        return parseInt(config.id) === moverlay_id;
      });
      state.configurator.configs[index].material.map(mul_mat => {
       if(!isActive){
         if (mul_mat.material_id === payload.material_id) 
           mul_mat.active = true;
         return mul_mat;
       }
       else if(isActive) {
         if (mul_mat.material_id === payload.material_id)
           mul_mat.active = false;
         return mul_mat;
       }  
      });
    },
    SET_SOCIAL_URL: function(state, payload) {
      state.social.url = payload;
    }
  },

  actions: {
    set_configuration({ commit }) {
      axios.get("./php/api.php").then(response => {
        commit("SET_CONFIGURATION", response.data);
      });

      axios
        .get("./Configurations/social.json")
        .then(response => commit("SET_SOCIAL", response.data));
    },
    set_configurator({ commit }, payload) {
      commit("SET_CONFIGURATOR", payload);
    },
    set_active({ commit }, payload) {
      commit("SET_ACTIVE", payload);
    },
    change_combination({ commit }, payload) {
      commit("CHANGE_COMBINATION", payload);
    },
    set_social_url({ commit }, payload) {
      commit("SET_SOCIAL_URL", payload);
    },
    add_overlay_item({ commit }, payload) {
      commit("ADD_OVERLAY_ITEM", payload);
    }
  },

  getters: {
    configurations(state) {
      return state.configurations;
    },
    configurator(state) {
      return state.configurator;
    },
    has_preloaded(state) {
      return state.app_preloaded;
    },
    has_configurator(state) {
      return state.is_configurator_fetched;
    },
    has_overlay(state) {
      return state.configurator_overlay;
    },
    social(state) {
      return state.social;
    },
    active_set(state) {
      if (!state.is_configurator_undefined && state.is_configurator_selected)
        return state.configurator.configs.find(item => item.active);
    },
    combination(state) {
      if (state.is_configurator_undefined || !state.is_configurator_selected)
        return false;
      const non_overlay_configs = state.configurator.configs.filter(
        item => !item.isOverlay
      );
      return non_overlay_configs.reduce((acc, current) => {
        const active_material = current.material.find(mat => mat.active);
        return acc + (current.id + active_material.material_id);
      }, "");
    },
    excluded_overlays(state) {
      if (state.is_configurator_undefined || !state.is_configurator_selected)
        return false;

      let overlays = [];

      const non_overlay_configs = state.configurator.configs.filter(
        item => !item.isOverlay
      );

      const materials = non_overlay_configs.map(item => {
        return item.material.find(mat => mat.active);
      });

      materials.forEach(mat => {
        if (mat.excluded_overlays && mat.excluded_overlays !== null) {
          mat.excluded_overlays.map(ex => {
            if (!overlays.includes(ex)) overlays.push(ex);
          });
        }
      });

      return overlays;
    },
    type(state) {
      return state.type;
    },
    active_materials(state) {
      if (state.is_configurator_undefined || !state.is_configurator_selected)
        return false;
      const overlays = state.configurator.configs.filter(
        conf => conf.isOverlay
      );
      const mul_overlays = state.configurator.configs.filter(
        conf => conf.isMultiple
      );
      let overlay_paths = overlays.map(conf => {
        const id = conf.id;
        const material = conf.material.find(mat => mat.active);
        const material_id = material.material_id;
        return `./Configurations/${state.configurator.name}/overlays/${id +
          material_id}.png`;
      });
      return overlay_paths.sort((a, b) => {
        const first = a.replace(/[^\d]/g, "");
        const second = b.replace(/[^\d]/g, "");
        return parseInt(first) - parseInt(second);
      });
    },
    price(state) {
      if (state.is_configurator_undefined || !state.is_configurator_selected)
        return false;

      return state.configurator.configs.reduce((acc, current) => {
        const active_material = current.material.find(mat => mat.active);
        const mul_active_material = current.material.find(mul_mat => mul_mat.active);
        return acc + parseInt(active_material.price || 0);
      }, 0);
    },
    selected_materials(state) {
      if (state.is_configurator_undefined || !state.is_configurator_selected)
        return false;

      return state.configurator.configs.map(config => {
        return {
          name: config.name,
          material: config.material.find(item => item.active)
        };
      });
    }
  }
});
