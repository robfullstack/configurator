import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/builder",
      redirect: "/"
    },
    {
      path: "/builder/:type/:base?/:overlay?",
      name: "builder",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ "./views/Builder")
    },
    {
      path: "*",
      component: () => import("./views/NotFound")
    }
  ]
});
