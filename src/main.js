import "./styles/main.scss";
import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import VueRouter from "vue-router";
import "./registerServiceWorker";
import SocialSharing from "vue-social-sharing";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faFacebook } from "@fortawesome/free-brands-svg-icons";
import { faTwitter } from "@fortawesome/free-brands-svg-icons";
import { faLinkedin } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faEnvelope, faFacebook, faTwitter, faLinkedin);

Vue.use(SocialSharing);
Vue.use(VueRouter);
Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  created: () => {
    store.dispatch("set_configuration");
  },
  render: h => h(App)
}).$mount("#app");
